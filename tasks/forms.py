from django import forms
from tasks.models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ("is_completed",)
        fields = (
            "name",
            "start_date",
            "due_date",
            "is_completed",
            "project",
            "assignee",
        )
